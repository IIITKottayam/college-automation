const jwt = require('jsonwebtoken');

async function getJwt(userid,roleid) {
  let token;
  await jwt.sign({userid, roleid},'secretKey',{expiresIn:'30s'},(err,tkn)=>{
    token = tkn;
  });
  return token;
}

module.exports = getJwt;