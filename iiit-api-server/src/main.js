const express = require('express');
const bcrypt = require('bcrypt');
const getJwt = require('./getJwt.js');
const pool = require('./dbconfig.js');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
  extended:true
}));

const jwtSecretKey = "r4nd0mp4$$worDK[7";
const FailedAttemptLimit = 5;

app.get('/',(req,res)=>{
  res.send("IIITK Automation API under construction");
});

app.post('/login', (req,res) => {
  if(!req.body.user_id || !req.body.password){
    res.status(400).send("Empty data");
    return;
  }
  const req_id = req.body.user_id;
  const req_password = req.body.password;
  console.log(req.body);
  pool.getConnection((err, conn) => {
    if (err) {
      console.log("Unable to connect");
      res.sendStatus(500);
    } else { // connected to db
      let qry = "SELECT * FROM User_Login WHERE email_id = ? OR user_id = ?";
      conn.query(qry, [req_id, req_id], (qryerr, records, fields) => {
        if (qryerr) {
          console.log("Unable to Query - User");
          res.sendStatus(500);
        } else {
          if (records.length !=1) {                                                               //user doesn't exist
          console.log(req_id,"User does not exist");
          res.status(401).send("User doesn't exist");
          } else {                                                                                  //user exists
            const {user_id, password, attempts, role_id} = records[0];
            if (isNaN(parseInt(attempts))) {                                                         //it's a date
              const oldDate = new Date(attempts)
              const diff = new Date() - oldDate;
              if (diff<=24*60*60*1000) {                                                            //user still not allowed
                console.log(user_id,"Can't allow, user is blocked");
                res.status(401).send("Blocked for 24 hours since last 5 consecutive failed attempts.");
              } else {                                                                              //let's reset
                bcrypt.compare(req_password, password, (hasherr, hashres) => {                      //24hrs passed and re-attempting
                  if (hasherr) {
                    console.log("Bcrypt error");
                    res.sendStatus(500);
                  } else {
                    if (hashres) {                                                                  //valid user
                      qry = "UPDATE User_Login SET attempts = '0' WHERE user_id = ?";
                      conn.query(qry, [user_id], (qryerr, records, fields) => {
                        if (qryerr) {
                          console.log("Could not reset attempt");
                          res.sendStatus(500);
                        } else {
                          console.log("Valid User");
                          //SEND JWT
                          getJwt(user_id, role_id)
                            .then((token) => {
                              res.send({token});
                            });
                        }
                      });
                    } else {                                                                       //Invalid User
                      qry = "UPDATE User_Login SET attempts = '1' WHERE user_id = ?";
                      conn.query(qry, [user_id], (qryerr, records, fields) => {
                        if (qryerr) {
                          console.log("Could not set attempt");
                          res.sendStatus(500);
                        } else {
                          console.log(user_id,"Wrong password");
                          res.sendStatus(401);
                        }
                      });
                    }
                  }
                });
              }
            } else {                                                                                 //it's a number
              bcrypt.compare(req_password, password, (hasherr, hashres) => {
                if (hasherr) {
                  console.log('Bcrypt error');
                  res.sendStatus(500);
                } else {
                  if (hashres) {                                                                    //Valid Password
                    if (parseInt(attempts) != 0) {
                      qry = "UPDATE User_Login SET attempts = '0' WHERE user_id = ?";
                      conn.query(qry, [user_id], (qryerr, records, fields) => {
                        if (qryerr) {
                          console.log("Could not reset attempt");
                          res.sendStatus(500);
                        } else {
                          console.log("Attempt reset");
                        }
                      });
                    }
                    console.log('Valid');
                    //SEND JWT
                    getJwt(user_id, role_id)
                      .then((token) => {
                        res.send({token});
                      });
                  } else {                                                                        //Invalid Password
                    if (parseInt(attempts)+1 < FailedAttemptLimit) {
                      let failed = (parseInt(attempts)+1).toString();
                      qry = "UPDATE User_Login SET attempts = ? WHERE user_id = ?";
                      conn.query(qry, [failed, user_id], (qryerr, records, fields) => {
                        if (qryerr) {
                          console.log("Could not increase attempt");
                          res.sendStatus(500);
                        } else {
                          console.log("Attempt increased");
                          res.status(401).send("Try again");
                        }
                      });
                    } else {                                                   ;                //Consecutive 5 fails
                      let date = (new Date()).toString();                                       //Let's set the date
                      qry = "UPDATE User_Login SET attempts = ? WHERE user_id = ?";
                      conn.query(qry, [date, user_id], (qryerr, records, fields) => {
                        if (qryerr) {
                          console.log("Could not set date");
                          res.sendStatus(500);
                        } else {
                          console.log("Blocked for 24hrs");
                          res.status(401).send("Blocked for 24 hrs");
                        }
                      });
                    }
                  }
                }
              });
            }
          }
        }
      });
      conn.release();
    }
  });
});

app.get('/test',(req,res) => {
  pool.getConnection((err, conn)=>{
    if(err) {
      res.sendStatus(500);
    } else {
      const qry = "SELECT * FROM User_Login";

      conn.query(qry, (err2, records, fields)=>{
        if(!err2) {
          console.log(records[0].email_id);
          res.send(records);
        }
        conn.release();
      });
    }
  });
});

//PORT
const port = process.env.PORT || 5000;
app.listen(port, ()=> {
  console.log(`Listening on port ${port}`);
});