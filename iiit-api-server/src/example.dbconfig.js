const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit:10,
  host:'localhost',
  user:'<< ENTER YOUR DB USERNAME >>',
  password:'<< ENTER YOUR DB PASSWORD >>',
  database:'iiit_automation'
});

module.exports = pool;