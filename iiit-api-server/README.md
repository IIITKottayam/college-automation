# College Automation

**API Backend**


**Developer's Guide :**
---
Create `dbconfig.js` file in `src` directory, and set your database username and password for your development environment.

*dbconfig.js* : ( How it should look like... )
```js
const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit:10,
  host:'localhost',
  user:'<< ENTER YOUR DB USERNAME >>',
  password:'<< ENTER YOUR DB PASSWORD >>',
  database:'iiit_automation'
});

module.exports = pool;
```

`example.dbconfig.js` file is present for reference.

----

Database in SQL format is stored inside `db` folder. Use same tables for code consistency. Update it in case new tables are added.

---
Install all node modules mentioned in `package.json` file using `npm install`.

To run project, use `npm start`.