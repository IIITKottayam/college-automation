import React, { Component } from 'react';
import Loader from '../components/Loader/Loader.jsx';
import './login.css';

class Login extends Component {

  state = {
    user_id : '',
    password: '',
    error:'',
    errorClass:'alert alert-light d-none',
    isChecking:false // checking authentication from api server
  };

  loginForm = (e) => {
    this.setState({isChecking:true});
    e.preventDefault();
    let http = new XMLHttpRequest();
    let url = 'http://localhost:5000/login';
    let params = `user_id=${this.state.user_id}&password=${this.state.password}`;

    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = () => {
        this.setState({isChecking:false});
        if(http.readyState === 4 && http.status === 200) {
          console.log(http.responseText);
          this.setState({error: 'Allowed access', errorClass: 'alert alert-success'});
        }else if(http.readyState === 4 && http.status !== 200){
          this.setState({error: http.responseText, errorClass: 'alert alert-danger'});
        }
    }
    http.send(params);
  }

  onChange = (e) => {
    this.setState({[e.target.name]:e.target.value});
  }

  render() {
    return (
    <div className="loginBox">
      <form onSubmit={this.loginForm}>
        <div className="form-group row">
          <div className="col-sm-12">
            <input
            name="user_id"
            type="text"
            className="form-control"
            id="staticEmail"
            value={this.state.user_id}
            onChange={this.onChange}
            placeholder="email@iiitkottayam.ac.in"
            />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-12">
            <div className="input-group">
              <input
              name="password"
              type="password"
              className="form-control"
              id="inputPassword"
              value={this.state.password}
              placeholder="Password"
              onChange={this.onChange}
              />
              <span className="input-group-btn">
                <button type="submit" className="btn btn-primary">
                  Login
                </button>
              </span>
            </div>
          </div>
        </div>
          {
            this.state.isChecking === true
            &&
            <Loader />
          }
          {
            this.state.isChecking === false
            &&
            <div className="row animated fadeIn info-message-box">
              <div className="col-sm-12">
                <div className={this.state.errorClass} role="alert">
                  {this.state.error}
                </div>
              </div>
            </div>
          }
      </form>
    </div>
    )
  }
}

export default Login;