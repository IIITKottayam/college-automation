import React, { Component } from 'react';
import loader from './loading.svg';
import './loader.css';

export default class Loader extends Component {
  render() {
    return (
      <img className="loader animated zoomIn" src={loader} alt="Loading.."/>
    )
  }
}
