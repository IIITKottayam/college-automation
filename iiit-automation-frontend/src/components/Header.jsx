import React, { Component } from 'react'

class Header extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
          <a className="text-light" href="#">
            <h3>
              IIIT Kottayam
            </h3>
          </a>
        </div>
      </nav>
    )
  }
}

export default Header;