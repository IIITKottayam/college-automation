import React, { Component } from 'react';
import Login from './containers/Login';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import Header from './components/Header';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div className="container">
          <Login />
        </div>
      </div>
    );
  }
}

export default App;